package main;

import main.trainable.Model;
import main.trainable.Trainable;
import main.trainable.TrainableNet;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Trainable trainable = new TrainableNet(new int[]{2, 2});

        Map<double[], double[]> dataSet = getDataSet();

        for (int i = 0; i < 10; i++)
            Trainable.train(dataSet, trainable);

        Model model = trainable.getModel();

        System.out.println(Arrays.toString(model.predict(new double[]{1, 0})));
        System.out.println(Arrays.toString(model.predict(new double[]{0, 1})));
    }

    public static Map<double[], double[]> getDataSet() {
        Map<double[], double[]> out = new HashMap<>();

        out.put(new double[]{1, 0}, new double[]{1, 4});
        out.put(new double[]{0, 1}, new double[]{-1, 0});

        return out;
    }
}
