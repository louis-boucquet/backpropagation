package main.neuralnet;

import main.numberfunctions.Matrix;
import main.numberfunctions.VectorHelp;
import main.trainable.Model;
import main.trainable.Trainable;

import java.util.Arrays;

public class NeuralLayer implements Model {
    private Matrix weights;

    private double[] bias;

    public NeuralLayer(Matrix weights, double[] bias) {
        this.weights = weights;
        this.bias = bias;
    }

    public double[] predict(double[] input) {
        // format input for matrix multiplication
        Matrix inputMatrix = new Matrix(new double[][]{input});

        double[] matrixResult = Matrix.product(inputMatrix, weights).getRow(0);

        // apply the bias
        return VectorHelp.sum(matrixResult, bias);
    }

    @Override
    public String toString() {
        return "TrainableLayer:" + '\n' +
                "\tweights=" + weights + '\n' +
                "\tbias=" + Arrays.toString(bias);
    }
}
