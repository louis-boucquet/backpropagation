package main.neuralnet;

import main.trainable.Model;

import java.util.List;
import java.util.stream.Collectors;

public class NeuralNet implements Model {
    private List<NeuralLayer> layers;

    public NeuralNet(List<NeuralLayer> layers) {
        this.layers = layers;
    }

    public double[] predict(double[] input) {
        // feedforward the input through the network
        for (NeuralLayer layer : layers)
            input = layer.predict(input);

        return input;
    }
    @Override
    public String toString() {
        return "TrainableNet\n" +
                    layers.stream().map(l -> "\t" + l.toString()).collect(Collectors.joining("\n"));
    }
}
