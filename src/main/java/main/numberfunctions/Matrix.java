package main.numberfunctions;

import java.util.Arrays;

public class Matrix {
    private double[][] values;

    public Matrix(double[][] values) {
        this.values = values;
    }

    public int getWidth() {
        return values[0].length;
    }

    public int getHeight() {
        return values.length;
    }

    public double get(int row, int column) {
        return values[row][column];
    }

    public void set(int row, int column, double value) {
        values[row][column] = value;
    }

    public void add(int row, int column, double value) {
        values[row][column] += value;
    }

    public double[] getRow(int row) {
        return values[row];
    }

    public void setRow(int row, double[] values) {
        this.values[row] = values;
    }

    public void addRow(int row, double[] values) {
        double[] newRow = VectorHelp.sum(getRow(row), values);
        setRow(row, newRow);
    }

    public double[] getColumn(int column) {
        double[] out = new double[getHeight()];

        for (int row = 0; row < getHeight(); row++) {
            out[row] = values[row][column];
        }

        return out;
    }

    public void setColumn(int column, double[] values) {
        for (int row = 0; row < values.length; row++) {
            this.values[row][column] = values[row];
        }
    }

    public void addColumn(int column, double[] values) {
        double[] newColumn = VectorHelp.sum(getColumn(column), values);
        setColumn(column, newColumn);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix = (Matrix) o;
        return Arrays.equals(values, matrix.values);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(values);
    }

    @Override
    public String toString() {
        return "Matrix{" + Arrays.deepToString(values) + '}';
    }

    public static Matrix zeroes(int width, int height) {
        double[][] values = new double[height][width];

        for (int i = 0; i < height; i++) {
            values[i] = new double[width];
        }

        return new Matrix(values);
    }

    public static Matrix ones(int width, int height) {
        double[][] values = new double[height][width];

        for (int i = 0; i < height; i++) {
            values[i] = new double[width];
            Arrays.fill(values[i], 1);
        }

        return new Matrix(values);
    }

    public static Matrix repeatRow(double[] row, int amount) {
        double[][] values = new double[amount][row.length];

        for (int i = 0; i < amount; i++)
            System.arraycopy(row, 0, values[i], 0, row.length);

        return new Matrix(values);
    }

    public static Matrix repeatColumn(double[] column, int amount) {
        double[][] values = new double[column.length][amount];

        for (int row = 0; row < column.length; row++)
            for (int columnIndex = 0; columnIndex < amount; columnIndex++)
                values[row][columnIndex] = column[row];

        return new Matrix(values);
    }

    public static Matrix product(Matrix m1, Matrix m2) {
        if (m1.getWidth() != m2.getHeight())
            throw new IllegalArgumentException("dimensions don't match");

        Matrix product = Matrix.zeroes(m2.getWidth(), m1.getHeight());

        for (int row = 0; row < product.getHeight(); row++) {
            for (int column = 0; column < product.getWidth(); column++) {
                double value = VectorHelp.dotProduct(m1.getRow(row), m2.getColumn(column));
                product.set(row, column, value);
            }
        }

        return product;
    }

    public static Matrix multiply(Matrix m, double amount) {
        double[][] values = new double[m.getHeight()][m.getWidth()];

        for (int row = 0; row < m.getHeight(); row++)
            for (int column = 0; column < m.getWidth(); column++)
                values[row][column] = m.get(row, column) * amount;

        return new Matrix(values);
    }

    public static Matrix productElementWise(Matrix m1, Matrix m2) {
        double[][] values = new double[m1.getHeight()][m1.getWidth()];

        for (int row = 0; row < m1.getHeight(); row++)
            for (int column = 0; column < m1.getWidth(); column++)
                values[row][column] = m1.get(row, column) * m2.get(row, column);

        return new Matrix(values);
    }

    public static Matrix sum(Matrix m1, Matrix m2) {
        if (m1.getWidth() != m2.getWidth() || m1.getHeight() != m2.getHeight())
            throw new IllegalArgumentException("dimensions don't match");

        Matrix sum = Matrix.zeroes(m1.getWidth(), m1.getHeight());

        for (int row = 0; row < sum.getHeight(); row++) {
            for (int column = 0; column < sum.getWidth(); column++) {
                double value = m1.get(row, column) + m2.get(row, column);
                sum.set(row, column, value);
            }
        }

        return sum;
    }
}
