package main.numberfunctions;

import java.util.Arrays;
import java.util.stream.DoubleStream;

public class VectorHelp {

    private VectorHelp() {
    }

    public static double[] sum(double[] v1, double[] v2) {
        double[] outVector = new double[v1.length];

        for (int i = 0; i < v1.length; i++) {
            outVector[i] = v1[i] + v2[i];
        }

        return outVector;
    }

    public static double sum(double[] v) {
        double out = 0;

        for (double e : v)
            out += e;

        return out;
    }

    public static double[] difference(double[] v1, double[] v2) {
        double[] outVector = new double[v1.length];

        for (int i = 0; i < v1.length; i++) {
            outVector[i] = v1[i] - v2[i];
        }

        return outVector;
    }

    public static double[] product(double[] v1, double[] v2) {
        double[] outVector = new double[v1.length];

        for (int i = 0; i < v1.length; i++) {
            outVector[i] = v1[i] * v2[i];
        }

        return outVector;
    }

    public static double[] product(double[] v1, double d) {
        double[] outVector = new double[v1.length];

        for (int i = 0; i < v1.length; i++) {
            outVector[i] = v1[i] * d;
        }

        return outVector;
    }

    public static double dotProduct(double[] v1, double[] v2) {
        double out = 0;

        for (int i = 0; i < v1.length; i++) {
            out += v1[i] * v2[i];
        }

        return out;
    }

    public static double[] ones(int outputs) {
        double[] out = new double[outputs];

        Arrays.fill(out, 1);

        return out;
    }

    public static double max(double[] doubles) {
        return DoubleStream
                .of(doubles)
                .max()
                .orElse(0);
    }

    public static double[] normalize(double[] doubles) {
        double max = max(doubles);

        if (max == 0)
            return new double[doubles.length];

        return DoubleStream
                .of(doubles)
                .map(d -> d / max)
                .toArray();
    }
}
