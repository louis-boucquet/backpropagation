package main.trainable;

public interface Model {
    double[] predict(double[] input);
}
