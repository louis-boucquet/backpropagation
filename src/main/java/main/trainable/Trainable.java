package main.trainable;

import org.w3c.dom.events.MouseEvent;

import java.util.Map;

/**
 * Trainable is meant for models with gradient descent
 */
public interface Trainable extends Model {

    /**
     * Update the correction for the model
     *
     * @param input          the input
     * @param expectedOutput the output that this input should cause
     */
    void updateCorrection(double[] input, double[] expectedOutput);

    /**
     * Apply the correction
     *
     * @param amount how extreme the correction will be
     */
    void applyCorrection(double amount);


    /**
     *
     * @return trained model
     */
    Model getModel();

    /**
     * Train with a specified amount
     *
     * @param dataSet the data to which you are trying to fit your model
     * @param trainable the Model
     * @param amount the learning rate
     */
    static void train(Map<double[], double[]> dataSet, Trainable trainable, double amount) {

        for (Map.Entry<double[], double[]> entry : dataSet.entrySet())
            trainable.updateCorrection(entry.getKey(), entry.getValue());

        trainable.applyCorrection(amount);
    }

    /**
     * Here the learning rate is chosen automatically
     * @param dataSet the dataset
     * @param trainable the model
     */
    static void train(Map<double[], double[]> dataSet, Trainable trainable) {
        train(dataSet, trainable, 1d / dataSet.size());
    }
}
