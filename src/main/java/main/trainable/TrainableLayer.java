package main.trainable;

import main.neuralnet.NeuralLayer;
import main.numberfunctions.Matrix;
import main.numberfunctions.VectorHelp;

import java.util.Arrays;

public class TrainableLayer implements Trainable {
    private Matrix weights;
    private Matrix deltaWeights;

    private double[] bias;
    private double[] deltaBias;

    public TrainableLayer(int inputs, int outputs) {
        this.weights = Matrix.ones(outputs, inputs);
        this.deltaWeights = Matrix.zeroes(outputs, inputs);

        this.bias = new double[outputs];
        this.deltaBias = new double[outputs];
    }

    public double[] predict(double[] input) {
        // format input for matrix multiplication
        Matrix inputMatrix = new Matrix(new double[][]{input});

        double[] matrixResult = Matrix.product(inputMatrix, weights).getRow(0);

        // apply the bias and return the sum
        // return the prediction
        return VectorHelp.sum(matrixResult, bias);
    }

    public void updateCorrection(double[] input, double[] expectedOutput) {
        double[] actualOutput = predict(input);

        // calculate the difference between actual and expected
        double[] difference = VectorHelp.difference(expectedOutput, actualOutput);

        // correct for the difference and
        updateGradient(input, difference);
    }

    public double[] updateGradient(double[] input, double[] differenceOutput) {
        // update the bias gradient
        deltaBias = VectorHelp.sum(deltaBias, differenceOutput);

        // we want to work with the normalized input
        input = VectorHelp.normalize(input);

        // applying the difference from the input
        Matrix differenceWeight = Matrix.repeatRow(differenceOutput, input.length);

        // moderate the difference according to the strength of the input
        Matrix inputWeight = Matrix.repeatColumn(input, differenceOutput.length);

        // combine the two
        Matrix weights = Matrix.productElementWise(differenceWeight, inputWeight);

        // add correction
        deltaWeights = Matrix.sum(deltaWeights, weights);

        return getThroughPut(weights);
    }

    public void applyCorrection(double amount) {
        // multiply by the amount
        deltaWeights = Matrix.multiply(deltaWeights, amount);

        // add the correction to the weights
        weights = Matrix.sum(weights, deltaWeights);

        // reset the correction, it's already applied
        deltaWeights = Matrix.zeroes(deltaWeights.getWidth(), deltaWeights.getHeight());

        // multiply by the amount
        deltaBias = VectorHelp.product(deltaBias, amount);

        // apply deltaBias
        bias = VectorHelp.sum(deltaBias, bias);
    }

    @Override
    public Model getModel() {
        return new NeuralLayer(weights, bias);
    }

    @Override
    public String toString() {
        return "TrainableLayer:" + '\n' +
                "\tweights=" + weights + '\n' +
                "\tbias=" + Arrays.toString(bias);
    }

    private static double[] getThroughPut(Matrix correctionWeights) {

        double[] out = new double[correctionWeights.getHeight()];

        for (int i = 0; i < correctionWeights.getHeight(); i++)
            out[i] = VectorHelp.sum(correctionWeights.getRow(i));

        return out;
    }
}
