package main.trainable;

import main.neuralnet.NeuralLayer;
import main.neuralnet.NeuralNet;
import main.numberfunctions.VectorHelp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TrainableNet implements Trainable {
    private List<TrainableLayer> layers = new ArrayList<>();

    public TrainableNet(int[] dimensions) {
        for (int i = 1; i < dimensions.length; i++)
            layers.add(new TrainableLayer(dimensions[i - 1], dimensions[i]));
    }

    public double[] predict(double[] input) {
        // feedforward the input through the network
        for (TrainableLayer layer : layers)
            input = layer.predict(input);

        return input;
    }

    public void updateCorrection(double[] input, double[] expectedOutput) {
        // inputs per layer
        List<double[]> inputs = new ArrayList<>();

        // feedforward the input and remember the intermediateInputs
        for (TrainableLayer layer : layers) {
            inputs.add(input);
            input = layer.predict(input);
        }

        double[] difference = VectorHelp.difference(expectedOutput, input);

        for (int i = layers.size() - 1; i > -1; i--) {
            TrainableLayer layer = layers.get(i);
            double[] intermediateInput = inputs.get(i);

            difference = layer.updateGradient(intermediateInput, difference);
        }
    }

    public void applyCorrection(double amount) {
        layers.forEach(layer -> layer.applyCorrection(amount));
    }

    @Override
    public Model getModel() {
        List<NeuralLayer> layers = this.layers
                .stream()
                .map(l -> (NeuralLayer) l.getModel())
                .collect(Collectors.toList());
        return new NeuralNet(layers);
    }

    @Override
    public String toString() {
        return "TrainableNet\n" +
                    layers.stream().map(l -> "\t" + l.toString()).collect(Collectors.joining("\n"));
    }
}
