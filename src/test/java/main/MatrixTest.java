package main;

import main.numberfunctions.Matrix;
import org.junit.Test;

import static org.junit.Assert.*;

public class MatrixTest {

    @Test
    public void multiply() {
        Matrix m1 = new Matrix(new double[][]{
                new double[]{1, 2}
        });

        Matrix m2 = new Matrix(new double[][]{
                new double[]{1, 2},
                new double[]{3, 4}
        });

        Matrix expected = new Matrix(new double[][]{
                new double[]{1 + 2*3, 2 + 2*4}
        });

//        assertEquals(expected, Matrix.multiply(m1, m2));
    }

    @Test
    public void add() {
        Matrix m1 = new Matrix(new double[][]{
                new double[]{1, 2},
                new double[]{3, 4}
        });

        Matrix m2 = new Matrix(new double[][]{
                new double[]{1, 2},
                new double[]{3, 4}
        });

        Matrix expected = new Matrix(new double[][]{
                new double[]{2, 4},
                new double[]{6, 8}
        });

        System.out.println(Matrix.sum(m1, m2));

//        assertEquals(expected, Matrix.sum(m1, m2));
    }

    @Test
    public void singleValue() {
        Matrix matrix = Matrix.zeroes(2, 3);

        assertEquals(matrix.get(0, 0), 0, 0.001);

        matrix.set(0, 0, 1);
        assertEquals(matrix.get(0, 0), 1, 0.001);

        matrix.add(0, 0, 1);
        assertEquals(matrix.get(0, 0), 2, 0.001);
    }

    @Test
    public void row() {
        Matrix matrix = Matrix.zeroes(2, 3);

        assertArrayEquals(new double[]{0, 0}, matrix.getRow(0), 0.001);

        matrix.setRow(0, new double[]{1, 2});
        assertArrayEquals(new double[]{1, 2}, matrix.getRow(0), 0.001);

        matrix.addRow(1, new double[]{1, 2});
        assertArrayEquals(new double[]{1, 2}, matrix.getRow(1), 0.001);
    }

    @Test
    public void column() {
        Matrix matrix = Matrix.zeroes(2, 3);

        assertArrayEquals(new double[]{0, 0, 0}, matrix.getColumn(0), 0.001);

        matrix.setColumn(0, new double[]{1, 2, 3});
        assertArrayEquals(new double[]{1, 2, 3}, matrix.getColumn(0), 0.001);

        matrix.addColumn(1, new double[]{1, 2, 3});
        assertArrayEquals(new double[]{1, 2, 3}, matrix.getColumn(1), 0.001);
    }

    @Test
    public void testToString() {
        System.out.println(Matrix.zeroes(2, 3));
    }

    @Test
    public void zeroes() {
        Matrix.zeroes(2, 3);
    }
}