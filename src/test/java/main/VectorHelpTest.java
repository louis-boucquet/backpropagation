package main;

import main.numberfunctions.VectorHelp;
import org.junit.Test;

import static org.junit.Assert.*;

public class VectorHelpTest {

    @Test
    public void sum() {
        assertArrayEquals(
                new double[]{2, 2, 2},
                VectorHelp.sum(
                        new double[]{0, 1, 2},
                        new double[]{2, 1, 0}
                ),
                0.001
        );
    }

    @Test
    public void difference() {
        assertArrayEquals(
                new double[]{-2, 0, 2},
                VectorHelp.difference(
                        new double[]{0, 1, 2},
                        new double[]{2, 1, 0}
                ),
                0.001
        );
    }

    @Test
    public void product() {
        assertArrayEquals(
                new double[]{0, 1, 0},
                VectorHelp.product(
                        new double[]{0, 1, 2},
                        new double[]{2, 1, 0}
                ),
                0.001
        );
    }

    @Test
    public void dotProduct() {
        assertEquals(
                1d,
                VectorHelp.dotProduct(
                        new double[]{0, 1, 2},
                        new double[]{2, 1, 0}
                ),
                0.001
        );
    }
}